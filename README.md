# EP1 - Urna ELetrônica

Projeto elaborado por: Ian Pereira de Sousa Rocha
Matrícula UnB: 16/0124778
Professor: Renato Sampaio
Matéria: Orientação a Objetos 2º/2018


Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto

O projeto armazena um numero variavel de eleitores e seus respectivos nomes.
Possui todo o loop de votação para todos os candidatos, podendo ser escolhido se quer ou não votar em especificamente uma categoria ou não.
Possui opções de Nulo e Branco.
Possui também oções para confirmar ou corrigir o voto.
Apresenta no final um relatório dizendo especificamente em quem cada eleitor votou.

## Bugs e problemas

Infelizmente ele não está lendo nomes compostos por problema no buffer.

## Referências
