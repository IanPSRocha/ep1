#ifndef SENADOR_HPP
#define SENADOR_HPP

#include <string>

using namespace std;

class Senador{
private:
  string nome_completo;
  string nome_urna;
  string cargo;
  string sigla_partido;
  string nome_partido;
  string numero_partido;
  string numero_candidato;
  string primeiro_suplente;
  string segundo_suplente;
public:
  Senador();
  ~Senador();
  string get_nome_completo();
    void set_nome_completo(string nome_completo);
  string get_nome_urna();
    void set_nome_urna(string nome_urna);
  string get_cargo();
    void set_cargo(string cargo);
  string get_sigla_partido();
    void set_sigla_partido(string sigla_partido);
  string get_nome_partido();
    void set_nome_partido(string nome_partido);
  string get_numero_partido();
    void set_numero_partido(string numero_partido);
  string get_numero_candidato();
    void set_numero_candidato(string numero_candidato);
  string get_primeiro_suplente();
    void set_primeiro_suplente(string primeiro_suplente);
  string get_segundo_suplente();
    void set_segundo_suplente(string segundo_suplente);
  void imprime_dados_sen();
};

#endif
