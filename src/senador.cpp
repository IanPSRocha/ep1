#include "senador.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Senador::Senador(){
  nome_completo = "";
  nome_urna = "Candidato Inválido";
  cargo = "";
  sigla_partido = "-";
  nome_partido = "-------";
  numero_partido = "";
  numero_candidato = "";
  primeiro_suplente = "Não Possui";
  segundo_suplente = "Não Possui";
}

Senador::~Senador(){};

string Senador::get_nome_completo(){
  return nome_completo;
}
void Senador::set_nome_completo(string nome_completo){
  this->nome_completo = nome_completo;
}
string Senador::get_nome_urna(){
  return nome_urna;
}
void Senador::set_nome_urna(string nome_urna){
  this->nome_urna = nome_urna;
}
string Senador::get_cargo(){
  return cargo;
}
void Senador::set_cargo(string cargo){
  this->cargo = cargo;
}
string Senador::get_sigla_partido(){
  return sigla_partido;
}
void Senador::set_sigla_partido(string sigla_partido){
  this->sigla_partido = sigla_partido;
}
string Senador::get_nome_partido(){
  return nome_partido;
}
void Senador::set_nome_partido(string nome_partido){
  this->nome_partido = nome_partido;
}
string Senador::get_numero_partido(){
  return numero_partido;
}
void Senador::set_numero_partido(string numero_partido){
  this->numero_partido = numero_partido;
}
string Senador::get_numero_candidato(){
  return numero_candidato;
}
void Senador::set_numero_candidato(string numero_candidato){
  this->numero_candidato = numero_candidato;
}
string Senador::get_primeiro_suplente(){
  return primeiro_suplente;
}
void Senador::set_primeiro_suplente(string primeiro_suplente){
  this->primeiro_suplente = primeiro_suplente;
}
string Senador::get_segundo_suplente(){
  return segundo_suplente;
}
void Senador::set_segundo_suplente(string segundo_suplente){
  this->segundo_suplente = segundo_suplente;
}
void Senador::imprime_dados_sen(){
  string num;
  string aux1 = "5";
  string aux2 = "9";
  string aux3 = "10";

  ifstream ip("../data/consulta_cand_2018_DF-melhorada.csv");

  if(!ip.is_open()) cout << "ERROR: File Open" << endl;

  string sen[1238][8];

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(j == 7){
        getline(ip, sen[i][j], '\n');
      }
      else{
        getline(ip, sen[i][j], ',');
      }
    }
  }

  num = get_numero_candidato();

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(num == sen[i][2] && aux1 == sen[i][0]){
        set_nome_completo(sen[i][3]);
        set_nome_urna(sen[i][4]);
        set_numero_partido(sen[i][5]);
        set_sigla_partido(sen[i][6]);
        set_nome_partido(sen[i][7]);
      }
      if(num == sen[i][2] && aux2 == sen[i][0]){
        set_primeiro_suplente(sen[i][4]);
      }
      if(num == sen[i][2] && aux3 == sen[i][0]){
        set_segundo_suplente(sen[i][4]);
      }
    }
  }

  cout << endl << "Nome: " << endl << get_nome_urna() << endl;
  cout << "Partido: " << endl << get_nome_partido() << " (" << get_sigla_partido() << ") - " << get_numero_partido() << endl;
  cout << endl << "1 Suplente: " << endl << get_primeiro_suplente() << endl;
  cout << endl << "2 Suplente: " << endl << get_segundo_suplente() << endl;

  ip.close();
}
