#include "candidato.hpp"
#include <iostream>
#include <string>

using namespace std;

Candidato::Candidato(){
  nome = "";
  cargo = "";
  sigla_partido = "";
  nome_partido = "";
  numero = "";
}

Candidato::~Candidato(){}

string Candidato::get_nome(){
  return nome;
}
void Candidato::set_nome(string nome){
  this->nome = nome;
}
string Candidato::get_cargo(){
  return cargo;
}
void Candidato::set_cargo(string cargo){
  this->cargo = cargo;
}
string Candidato::get_sigla_partido(){
  return sigla_partido;
}
void Candidato::set_sigla_partido(string sigla_partido){
  this->sigla_partido = sigla_partido;
}
string Candidato::get_nome_partido(){
  return nome_partido;
}
void Candidato::set_nome_partido(string nome_partido){
  this->nome_partido = nome_partido;
}
string Candidato::get_numero(){
  return numero;
}
void Candidato::set_numero(string numero){
  this->numero = numero;
}
