#include <iostream>
#include "candidato.hpp"
#include "dep_fed.hpp"
#include "eleitor.hpp"
#include "dep_dis.hpp"
#include "governador.hpp"
#include "presidente.hpp"
#include "senador.hpp"
#include <string>
#include <fstream>
#include <cstring>

using namespace std;

int main(int argc, char ** argv){
  int n;
  cout << "Digite quantas pessoas participarão da Votação" << endl;
  cin >> n;
  string nome[n];
  string votos[n][5];

  for(int i = 0; i < n; i++){
    Eleitor * eleitor = new Eleitor();
    cout << endl << endl << endl << "Eleitor número " << i+1 << ", Por favor digite seu nome abaixo:" << endl;
    cin >> nome[i];
    eleitor->set_nome(nome[i]);

    //DEPUTADO FEDERAL
    int decis1 = 1;
    do{
      cout << endl << endl << "Você gostaria de Votar para Deputado Federal?" << endl;
      cout << "[1] - Sim" << endl << "[2] - Não" << endl;
      cin >> decis1;

      while(decis1 != 2 && decis1 != 1){
        cout << endl << "Opção Inválida, por favor escolha novamente" << endl;
        cout << "[1] - Sim" << endl << "[2] - Não" << endl;
        cin >> decis1;
      }
      if(decis1 == 2){
        votos[i][0] = "Não votou!";
        break;
      }
      else{
        cout << endl << "Digite o número do seu candidato para Deputado Federal" << endl;
        cout << "[0] - Nulo" << endl;
        cout << "[1] - Branco" << endl;
        string num_dep_fed;
        cin >> num_dep_fed;
        if(num_dep_fed == "0" || num_dep_fed == "1"){
          if(num_dep_fed == "0"){
            votos[i][0] = "Voto Nulo";
          }
          else{
            votos[i][0] = "Voto em Branco";
          }
          break;
        }
        else{
          int confirma = 2;
          do{
            Dep_Federal * dep_federal = new Dep_Federal();
            dep_federal->set_numero_candidato(num_dep_fed);
            dep_federal->imprime_dados_fed();

            cout << "[2] - Confirma" << endl;
            cout << "[3] - Corrige" << endl;
            cin >> confirma;

            while(confirma != 2 && confirma != 3){
              cout << "Opção Inválida" << endl;
              cout << "[2] - Confirma" << endl;
              cout << "[3] - Corrige" << endl;
              cin >> confirma;
            }

            if(confirma == 3){
              cout << "Digite Novamente" << endl;
              cin >> num_dep_fed;
              confirma--;
            }

            else{
              cout << endl << "Voto Confirmado!" << endl;
              votos[i][0] = dep_federal->get_nome_urna();
              confirma = 0;
            }
          }while(confirma == 2);
        }
      }
        decis1++;
    }while(decis1 != 2);

    //DEPUTADO DISTRITAL
    int decis2 = 1;
    do{
      cout << endl << endl << "Você gostaria de Votar para Deputado Distrital?" << endl;
      cout << "[1] - Sim" << endl << "[2] - Não" << endl;
      cin >> decis2;

      while(decis2 != 2 && decis2 != 1){
        cout << "Opção Inválida, por favor escolha novamente" << endl;
        cout << "[1] - Sim" << endl << "[2] - Não" << endl;
        cin >> decis2;
      }
      if(decis2 == 2){
        votos[i][1] = "Não votou!";
        break;
      }
      else{
        cout << endl << endl << "Digite o número do seu candidato para Deputado Distrital" << endl;
        cout << "[0] - Nulo" << endl;
        cout << "[1] - Branco" << endl;
        string num_dep_dis;
        cin >> num_dep_dis;
        if(num_dep_dis == "0" || num_dep_dis == "1"){
          if(num_dep_dis == "0"){
            votos[i][1] = "Voto Nulo";
          }
          else{
            votos[i][1] = "Voto em Branco";
          }
          break;
        }
        else{
          int confirma = 2;
          do{
            Dep_Distrital * dep_distrital = new Dep_Distrital();
            dep_distrital->set_numero_candidato(num_dep_dis);
            dep_distrital->imprime_dados_dis();

            cout << "[2] - Confirma" << endl;
            cout << "[3] - Corrige" << endl;
            cin >> confirma;

            while(confirma != 2 && confirma != 3){
              cout << "Opção Inválida" << endl;
              cout << "[2] - Confirma" << endl;
              cout << "[3] - Corrige" << endl;
              cin >> confirma;
            }

            if(confirma == 3){
              cout << "Digite Novamente" << endl;
              cin >> num_dep_dis;
              confirma--;
            }

            else{
              cout << endl << "Voto Confirmado!" << endl;
              votos[i][1] = dep_distrital->get_nome_urna();
              confirma = 0;
            }
          }while(confirma == 2);
        }
      }
        decis2++;
    }while(decis2 != 2);

    //Senador
    int decis3 = 1;
    do{
      cout << endl << endl<< "Você gostaria de Votar para Senador?" << endl;
      cout << "[1] - Sim" << endl << "[2] - Não" << endl;
      cin >> decis3;

      while(decis3 != 2 && decis3 != 1){
        cout << "Opção Inválida, por favor escolha novamente" << endl;
        cout << "[1] - Sim" << endl << "[2] - Não" << endl;
        cin >> decis3;
      }
      if(decis3 == 2){
        votos[i][2] = "Não votou!";
        break;
      }
      else{
        cout << endl << endl << "Digite o número do seu candidato para Senador" << endl;
        cout << "[0] - Nulo" << endl;
        cout << "[1] - Branco" << endl;
        string num_senador;
        cin >> num_senador;
        if(num_senador == "0" || num_senador == "1"){
          if(num_senador == "0"){
            votos[i][2] = "Voto Nulo";
          }
          else{
            votos[i][2] = "Voto em Branco";
          }
          break;
        }
        else{
          int confirma = 2;
          do{
            Senador * senador = new Senador();
            senador->set_numero_candidato(num_senador);
            senador->imprime_dados_sen();

            cout << "[2] - Confirma" << endl;
            cout << "[3] - Corrige" << endl;
            cin >> confirma;

            while(confirma != 2 && confirma != 3){
              cout << "Opção Inválida" << endl;
              cout << "[2] - Confirma" << endl;
              cout << "[3] - Corrige" << endl;
              cin >> confirma;
            }

            if(confirma == 3){
              cout << "Digite Novamente" << endl;
              cin >> num_senador;
              confirma--;
            }

            else{
              cout << endl << "Voto Confirmado!" << endl;
              votos[i][2] = senador->get_nome_urna();
              confirma = 0;
            }
          }while(confirma == 2);
        }
      }
        decis3++;
    }while(decis3 != 2);

    //Governador
    int decis4 = 1;
    do{
      cout << endl << endl << "Você gostaria de Votar para Governador?" << endl;
      cout << "[1] - Sim" << endl << "[2] - Não" << endl;
      cin >> decis4;
      cout << decis4 << endl;

      while(decis4 != 2 && decis4 != 1){
        cout << "Opção Inválida, por favor escolha novamente" << endl;
        cout << "[1] - Sim" << endl << "[2] - Não" << endl;
        cin >> decis4;
      }
      if(decis4 == 2){
        votos[i][3] = "Não votou!";
        break;
      }
      else{
        cout << endl << endl << "Digite o número do seu candidato para Governador" << endl;
        cout << "[0] - Nulo" << endl;
        cout << "[1] - Branco" << endl;
        string num_gov;
        cin >> num_gov;
        if(num_gov == "0" || num_gov == "1"){
          if(num_gov == "0"){
            votos[i][3] = "Voto Nulo";
          }
          else{
            votos[i][3] = "Voto em Branco";
          }
          break;
        }
        else{
          int confirma = 2;
          do{
            Governador * governador = new Governador();
            governador->set_numero_candidato(num_gov);
            governador->imprime_dados_gov();

            cout << "[2] - Confirma" << endl;
            cout << "[3] - Corrige" << endl;
            cin >> confirma;

            while(confirma != 2 && confirma != 3){
              cout << "Opção Inválida" << endl;
              cout << "[2] - Confirma" << endl;
              cout << "[3] - Corrige" << endl;
              cin >> confirma;
            }

            if(confirma == 3){
              cout << "Digite Novamente" << endl;
              cin >> num_gov;
              confirma--;
            }

            else{
              cout << endl << "Voto Confirmado!" << endl;
              votos[i][3] = governador->get_nome_urna();
              confirma = 0;
            }
          }while(confirma == 2);
        }
      }
        decis4++;
    }while(decis4 != 2);

    //Presidente
    int decis5 = 1;
    do{
      cout << endl << endl <<"Você gostaria de Votar para Presidente?" << endl;
      cout << "[1] - Sim" << endl << "[2] - Não" << endl;
      cin >> decis5;
      cout << decis5 << endl;

      while(decis5 != 2 && decis5 != 1){
        cout << "Opção Inválida, por favor escolha novamente" << endl;
        cout << "[1] - Sim" << endl << "[2] - Não" << endl;
        cin >> decis5;
      }
      if(decis5 == 2){
        votos[i][4] = "Não votou!";
        break;
      }
      else{
        cout << endl<< endl << "Digite o número do seu candidato para Presidente" << endl;
        cout << "[0] - Nulo" << endl;
        cout << "[1] - Branco" << endl;
        string num_pre;
        cin >> num_pre;
        if(num_pre == "0" || num_pre == "1"){
          if(num_pre == "0"){
            votos[i][4] = "Voto Nulo";
          }
          else{
            votos[i][4] = "Voto em Branco";
          }
          break;
        }
        else{
          int confirma = 2;
          do{
            Presidente * presidente = new Presidente();
            presidente->set_numero_candidato(num_pre);
            presidente->imprime_dados_pre();

            cout << "[2] - Confirma" << endl;
            cout << "[3] - Corrige" << endl;
            cin >> confirma;

            while(confirma != 2 && confirma != 3){
              cout << "Opção Inválida" << endl;
              cout << "[2] - Confirma" << endl;
              cout << "[3] - Corrige" << endl;
              cin >> confirma;
            }

            if(confirma == 3){
              cout << "Digite Novamente" << endl;
              cin >> num_pre;
              confirma--;
            }

            else{
              cout << endl << "Voto Confirmado!" << endl;
              votos[i][4] = presidente->get_nome_urna();
              confirma = 0;
            }
          }while(confirma == 2);
        }
      }
        decis5++;
    }while(decis5 != 2);
    cout << endl << endl << "Obrigado por exercer seu papel como cidadão!!" << endl;
  }
  for(int i = 0; i < n; i++){
    cout << endl << "O/a eleitor(a) "<< nome[i] << " votou nos candidatos: " << endl;
    for(int j = 0; j < 5; j++){
      switch(j){
        case 0:
        cout << "Deputado Federal: ";
        break;
        case 1:
        cout << "Deputado Distrital: ";
        break;
        case 2:
        cout << "Senador: ";
        break;
        case 3:
        cout << "Governador: ";
        break;
        case 4:
        cout << "Presidente: ";
        break;
      }
      cout << votos[i][j] << endl;
    }
  }
return 0;
}
